# [Cognitive Scale Coding Test](./exercise.md) - Starfleet Mine Clearing Exercise Evaluator

![Red Shirt Army Options](screenshot.png)

## Requirements
This project utilizes:
 - [node.js](https://nodejs.org/download/)

## Install:

1. Clone this repo locally
1. cd into app dir
1. Run `$ npm install`
1. Build `$ npm run build`

## Run

1. In app dir run `$ DEBUG=cs-exercise:* npm start`
1. Open http://localhost:3000

## Design

[Illustrated idea / design diagram of program in this Google Drawing here](https://docs.google.com/drawings/d/1wHTS9CKvuBKcBKpjh9dIFU43jZhXGZtFdIOQWUGbhjY/edit?usp=sharing)


