var gulp = require('gulp'),
    useref = require('gulp-useref'),
    gulpif = require('gulp-if'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    runSequence = require('run-sequence'),
    fs = require('fs-extra');

gulp.task('clean', function () {
    fs.removeSync('./views/javascripts/combined.js');
    fs.removeSync('./views/stylesheets/combined.css');
    fs.removeSync('./public');
    fs.mkdirsSync('./public');
});

gulp.task('copy', function () {
    fs.copySync('./src/images', './public/images', {clobber: true});
    fs.copySync('./src/favicon.ico', './public/favicon.ico', {clobber: true});
    fs.copySync('./src/javascripts/cors/jquery.xdr-transport.js',
        './public/javascripts/cors/jquery.xdr-transport.js', {clobber: true});
    fs.copySync('./views/javascripts/combined.js', './public/javascripts/combined.js', {clobber: true});
    fs.copySync('./views/stylesheets/combined.css', './public/stylesheets/combined.css', {clobber: true});
    fs.removeSync('./views/javascripts');
    fs.removeSync('./views/stylesheets');
});

gulp.task('build', function () {
    var assets = useref.assets();

    return gulp.src('src/index.ejs')
        .pipe(assets)
        .pipe(gulpif('*.js', uglify()))
        .pipe(gulpif('*.css', minifyCss()))
        .pipe(assets.restore())
        .pipe(useref())
        .pipe(gulp.dest('views'));
});

gulp.task('default', function() {
    runSequence(
        'clean',
        'build',
        'copy'
    );
});