var fs = require('fs-extra');

module.exports = function(router) {

    router.get('/output', function(req, res) {
        var ws = fs.createOutputStream(__dirname + '/../public/output/output.txt');
        ws.write('....\n');
        res.send('hello');
    });

    return router;
}
