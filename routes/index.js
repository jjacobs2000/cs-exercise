var express = require('express');
var router = express.Router();
var uploadManager = require('./uploadManager')(router);
var output = require('./output')(router);
var fs = require('fs-extra');
var instructions = "upload field.txt and script.txt files<br>" +
    "then press 'Run Simulator' button<br>" +
    "you can drag both files and drop them <br>" +
    "on the '+Add Files' button";

/* GET home page. */
router.get('/', function(req, res, next) {
  var ws = fs.createOutputStream(__dirname + '/../public/output/output.txt');
  ws.write(instructions);
  res.render('index', { title: 'Starfleet Mine Clearing Exercise Evaluator' });
});

module.exports = router;
