// config the uploader
var options = {
    tmpDir: __dirname + '/../public/uploaded/tmp',
    publicDir: __dirname + '/../public/uploaded',
    uploadDir: __dirname + '/../public/uploaded/files',
    uploadUrl: '/uploaded/files/',
    maxPostSize: 11000000000, // 11 GB
    minFileSize: 1,
    autoUpload: true,
    maxFileSize: 10000000000, // 10 GB
    acceptFileTypes: /field.txt|script.txt/i,
    // Files not matched by this regular expression force a download dialog,
    // to prevent executing any scripts in the context of the service domain:
    inlineFileTypes: /\.(txt)$/i,
    imageTypes: /\.(gif|jpe?g|png)$/i,
    imageVersions: {
        maxWidth: 80,
        maxHeight: 80
    },
    copyImgAsThumb: false,
    accessControl: {
        allowOrigin: '*',
        allowMethods: 'OPTIONS, HEAD, GET, POST, PUT, DELETE',
        allowHeaders: 'Content-Type, Content-Range, Content-Disposition'
    },
    storage : {
        type : 'local'
    },
    nodeStatic: {
        cache: 3600 // seconds to cache served files
    }
};

var uploader = require('blueimp-file-upload-expressjs')(options);
var fs = require('fs-extra');


module.exports = function(router) {
    router.get('/upload', function(req, res) {
        uploader.get(req, res, function(err, obj) {
            res.send(JSON.stringify(obj));
        });

    });

    router.post('/upload', function(req, res) {
        uploader.post(req, res, function(err, obj) {
            res.send(JSON.stringify(obj));
        });
    });

    router.delete('/uploaded/files/:name', function(req, res) {
        fs.copy(__dirname + '/../public/uploaded/files/' + req.params.name,
                __dirname + '/../public/uploaded/files/thumbnail/' + req.params.name, function (err)
        {
            if (err) return console.error(err);
            uploader.delete(req, res, function(err, obj) {
                res.send(JSON.stringify(obj));
            });
            console.log("success!")
        }) // copies file
    });

    return router;
}
